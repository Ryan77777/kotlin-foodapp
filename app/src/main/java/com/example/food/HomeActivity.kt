package com.example.food

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.LinearLayout

class HomeActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        val foodGyoza = findViewById<LinearLayout>(R.id.foodGyoza)

        foodGyoza.setOnClickListener{
            val detail = Intent(this, DetailsActivity::class.java)
            startActivity(detail)
        }
    }
}